<?php
  $sql		= "SELECT MAX(motor_kode) from tbl_motor";
  $carikode = $conn->query($sql);
  $datakode = mysqli_fetch_array($carikode);
  if($datakode){
    $nilaikode = substr($datakode[0], 3);
    $kode = (int) $nilaikode;
    $kode = $kode + 1;
    $hasilkode = "MTR".str_pad($kode, 3, "0", STR_PAD_LEFT);
  }
  else{
    $hasilkode = "MTR001";
  }
?>


<section class="content-header">
  <h1>
    Motor
    <small><?= date('D, d/m/Y');?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Motor</a></li>
    <li class="active">Tambah Motor</li>
  </ol>
</section>
<section class="content">
	<!-- general form elements disabled -->
	<div class="box box-danger">
	  <!-- /.box-header -->
	  <div class="box-body">
	  	<div class="col-md-10 col-md-offset-1"  style="padding-bottom: 30px;">
		    <form role="form" method="post" action="" enctype="multipart/form-data">
		      <div class="form-group">
		        <label for="kode">Kode Motor</label>
		        <input type="text" name="m_kode" class="form-control" id="kode" value="<?= $hasilkode; ?>" autocomplete="off" readonly>
		      </div>
		      <div class="form-group">
		        <label for="Merek">Merek</label>
		        <input type="text" name="m_merek" class="form-control" id="Merek" placeholder="Masukan Merek" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="Type">Type</label>
		        <input type="text" name="m_type" class="form-control" id="Type" placeholder="Masukan Type" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="Warna">Warna</label>
		        <input type="text" name="m_warna" class="form-control" id="Warna" placeholder="Masukan Warna" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="harga">Harga</label>
		        <input type="number" name="m_harga" class="form-control" id="harga" placeholder="Masukan Harga" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="img">Gambar</label>
		        <input type="file" name="gambar" class="form-control" id="img">
		        <small>*Extensi diperbolehkan = JPG/PNG</small><br>
		        <small>*Ukuran gambar maksimal 2MB</small>
		      </div>
		</div>
	  </div>
	  <!-- /.box-body -->
	  <!-- .box-footer -->
	  <div class="box-footer text-right">
	  	<div class="col-md-10 col-md-offset-1">
	  		<button type="reset" name="reset" class="btn btn-danger text-left"><i class="fa fa-remove"></i> Cancel</button>
	    	<button type="submit" name="tambah" class="btn btn-success text-right"><i class="fa fa-plus"></i> Tambah</button>
	    	</form>
	    </div>
	  </div>
	</div>
	<!-- /.box -->
</section>

<?php

if (isset($_POST['tambah'])) {

	$kode	= htmlspecialchars($_POST['m_kode']);
	$merek 	= htmlspecialchars($_POST['m_merek']);
	$type 	= htmlspecialchars($_POST['m_type']);
	$warna 	= htmlspecialchars($_POST['m_warna']);
	$harga 	= htmlspecialchars($_POST['m_harga']);
	$file 	= $_FILES['gambar'];
	// upload file
	$file_name	= $file['name'];
	$file_tmp	= $file['tmp_name'];
	$file_size	= $file['size'];
	$file_error	= $file['error'];
	// ekstensi
	$extensi 	= explode('.', $file_name);
	$extensi 	= strtoupper(end($extensi));
	$allowed	= array('PNG', 'JPG', 'JPEG');


	if($kode == "" || $merek == "" || $type == "" || $warna == "" || $harga == ""){
	    ?>
	        <script type="text/javascript">
	            alert("Data tidak boleh kosong !");
	            window.location.href ="?page=motor";
	        </script>
	    <?php
	}
	else{
		if (in_array($extensi, $allowed) === true) {
			if ($file_error === 0) {
				if ($file_size <= 2097152) {
					$file_name_new = uniqid('', true).'.'.$extensi;
					$path = './motor/img/' . $file_name_new;

					if (move_uploaded_file($file_tmp, $path)) {
						$sql = "INSERT INTO tbl_motor VALUES ('$kode','$merek','$type','$warna','$harga','$file_name_new')";
						$insert = $conn->query($sql);

						if ($insert > 0) {
							?>
								<script type="text/javascript">
									alert('Data berhasil ditambahkan.');
									window.location.href="?page=motor";
								</script>
							<?php
						} else {
							echo "Data gagal tersimpan !";
						}
						$conn->close();
					}
				} else{
					?>
						<script type="text/javascript">
							alert('Ukuran file terlalu besar !!!');
							window.location.href="?page=motor";
						</script>
					<?php
				}
			} else{
				?>
					<script type="text/javascript">
						alert('Error !!!');
						window.location.href="?page=motor";
					</script>
				<?php
			}
		}else{
			?>
				<script type="text/javascript">
					alert('Extensi tidak diperbolehkan !!!');
					window.location.href="?page=motor";
				</script>
			<?php
		}
	}

}

?>
