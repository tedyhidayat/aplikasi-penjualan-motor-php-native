<?php

// batas pagination
$batas = 5;
$halaman = @$_GET['halaman'];

if (empty($halaman)) {
	$posisi = 0;
	$halaman = 1;
} else {
	$posisi = ($halaman - 1) * $batas;
}

// var search

if (isset($_POST['search'])) {
	$keyword = $_POST['table_search'];
	$sql = "SELECT * FROM tbl_motor WHERE motor_kode LIKE '%$keyword%' or motor_merk LIKE '%$keyword%' or motor_harga LIKE '%$keyword%' ORDER BY motor_kode ASC LIMIT $posisi,$batas ";
	$read_motor = $conn->query($sql);
	$res = mysqli_num_rows($read_motor);
} else {
	$sql = "SELECT * FROM tbl_motor ORDER BY motor_kode ASC LIMIT $posisi,$batas";
	$read_motor = $conn->query($sql);
	$res = mysqli_num_rows($read_motor);
}

?>



<section class="content-header">
  <h1>
    Motor
    <small><?= date('D, d/m/Y');?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Motor</a></li>
    <li class="active">Data motor</li>
  </ol>
</section>
<section class="content">
	<div class="row">
	  <div class="col-xs-12">
	    <div class="box">
	      <div class="box-header" style="padding-bottom: 20px;">
	        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#tambah_motor" style="border-radius: 0px;"><i class="fa fa-user-plus"></i> Tambah Data</button>

	        <div class="box-tools">
				<form action="" method="post">
		          <div class="input-group input-group-sm" style="width: 170px;">
		            <input type="text" name="table_search" class="form-control pull-right" placeholder="Masukan keyword" autocomplete="off">

		            <div class="input-group-btn">
		              <button type="submit" name="search" class="btn btn-default"><i class="fa fa-search"></i></button>
		            </div>
		          </div>
				</form>
	        </div>
	      </div>
	      <!-- /.box-header -->
	      <div class="box-body table-responsive no-padding">
	        <table class="table table-hover table-striped">
	          <tr class="alert-danger" style="border-bottom: 2px solid gray;">
	            <th width="100">Kode Motor</th>
	            <th>Merek</th>
	            <th>Type</th>
	            <th>Warna</th>
	            <th>Harga</th>
	            <th>Gambar</th>
	            <th class="text-center">Opsi</th>
	          </tr>

		      <?php foreach( $read_motor as $data ) : ?>
	          <tr>
	            <td><?= htmlspecialchars($data["motor_kode"]); ?></td>
	            <td><?= htmlspecialchars($data["motor_merk"]); ?></td>
	            <td><?= htmlspecialchars($data["motor_type"]); ?></td>
	            <td><?= htmlspecialchars($data["motor_warna_pilihan"]); ?></td>
	            <td> Rp. <?= number_format($data['motor_harga'], 2, ',','.'); ?></td>
	            <td> <img class="img-thumbnail" width="100px" src="./motor/img/<?= $data['gambar']; ?>" alt="Motor pic"></td>
	            <td class="text-center">
	            	<a class="btn btn-primary btn-sm" href="?page=motor&action=update_motor&kd_motor=<?= $data['motor_kode']; ?>&foto=<?php echo $data['gambar'] ?>">
			        	<i class="glyphicon glyphicon-edit"></i>
			        </a>
					<a class="btn btn-danger btn-sm" href="?page=motor&action=delete_motor&kd_motor=<?= $data['motor_kode']; ?>&foto=<?php echo $data['gambar'] ?>" onclick="return confirm('Yakin ingin hapus data ?');">
						<i class="glyphicon glyphicon-trash"></i>
					</a>
	            </td>
	          </tr>
	          <?php endforeach; ?>
	        </table>
	      </div>
	      <!-- /.box-body -->
	      <!-- /.box-body -->

	      <?php
	      	$sql = "SELECT * FROM tbl_motor";
	      	$query = $conn->query($sql);
	      	$jml_data = mysqli_num_rows($query);
	      	$jml_halaman = ceil($jml_data/$batas);
	      ?>

	      <div class="box-footer clearfix">
	      	<span class="text-left">Jumlah data</span> <span class="label label-warning"><?= $jml_data;?></span>

			<ul class="pagination pagination-sm no-margin pull-right">
	      	<?php
	      		for ($i=1; $i <= $jml_halaman; $i++) {
	      			if ($i != $halaman) {
	      				?>
						<li><a href="?page=motor&halaman=<?=$i;?>"><?=$i;?></a></li>
	      				<?php
	      			} else {
	      				?>
						<li class="active"><a href="#"><?=$i;?></a></li>
	      				<?php
	      			}
	      		}
	      	?>
	        </ul>
	      </div>
	    </div>
	    <!-- /.box -->
	  </div>
	</div>


	<!-- Tambah data motor -->
	<div class="modal fade" id="tambah_motor">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-motorcycle"></i> Tambah data motor</h4>
	      </div>
	      <div class="modal-body">
	        <?php include 'create_motor.php'; ?>
	      </div>
	    </div>
	    <!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	<!-- /.tambah data motor -->

	<!-- Tambah data motor -->
	<div class="modal fade" id="update_motor">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-user-plus"></i> Tambah data motor</h4>
	      </div>
	      <div class="modal-body">
	        <?php include 'update_motor.php'; ?>
	      </div>
	    </div>
	    <!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	<!-- /.tambah data motor -->
</section>