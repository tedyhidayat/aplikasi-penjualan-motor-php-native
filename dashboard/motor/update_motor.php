<?php
	$id				= $_GET['kd_motor'];
	$foto 			= $_GET['foto'];
	$sql			= $conn->query("SELECT * FROM tbl_motor WHERE motor_kode ='$id'") or die (mysqli_error());
	$data 			= mysqli_fetch_array($sql);
?>


<section class="content-header">
  <h1>
    motor
    <small><?= date('D, d/m/Y');?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">motor</a></li>
    <li class="active">Edit data motor</li>
  </ol>
</section>

<section class="content">
	<!-- general form elements disabled -->
	<div class="box box-success">
	  <!-- /.box-header -->
	  <div class="box-body">
	  	<div class="col-md-10 col-md-offset-1"  style="padding-bottom: 30px;">
		    <form role="form" method="post" action="" enctype="multipart/form-data">
		      <div class="form-group has-warning">
		        <label for="kode">Kode Motor</label>
		        <input type="text" name="m_kode" class="form-control" id="kode" value="<?= $data["motor_kode"]; ?>" readonly autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="merk">Merek</label>
		        <input type="text" name="m_merek" class="form-control" id="merk" value="<?= htmlspecialchars_decode($data["motor_merk"]); ?>" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="type">Type</label>
		        <input type="text" name="m_type" class="form-control" id="type" value="<?= htmlspecialchars_decode($data["motor_type"]); ?>" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="warna">Warna</label>
		        <input type="text" name="m_warna" class="form-control" id="warna" value="<?= htmlspecialchars_decode($data["motor_warna_pilihan"]); ?>" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="harga">Harga</label>
		        <input type="text" name="m_harga" class="form-control" id="harga" value="<?= htmlspecialchars_decode($data["motor_harga"]); ?>" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="img">Gambar</label>
		        <img width="200" src="./motor/img/<?= $foto; ?>" alt="<?= $foto; ?>" class="thumbnail">
		        <input type="file" name="gambar" class="form-control" id="img">
		        <small>*Extensi diperbolehkan = JPG/PNG</small><br>
		        <small>*Ukuran gambar maksimal 2MB</small>
		      </div>
		</div>
	  </div>
	  <!-- /.box-body -->
	  <!-- .box-footer -->
	  <div class="box-footer text-right">
	  	<div class="col-md-10 col-md-offset-1">
	  		<a class="btn btn-danger text-left" href="?page=motor"><i class="fa fa-remove"></i> Cancel</a>
	    	<button type="submit" name="update" class="btn btn-primary text-right"><i class="fa fa-edit"></i> Edit</button>
	    	</form>
	    </div>
	  </div>
	</div>
	<!-- /.box -->
</section>

<?php

if (isset($_POST['update'])) {

	$foto 	= $_GET['foto'];

	$kode	= htmlspecialchars($_POST['m_kode']);
	$merek 	= htmlspecialchars($_POST['m_merek']);
	$type 	= htmlspecialchars($_POST['m_type']);
	$warna 	= htmlspecialchars($_POST['m_warna']);
	$harga 	= htmlspecialchars($_POST['m_harga']);
	$file 	= $_FILES['gambar'];
	// upload file
	$file_name	= $file['name'];
	$file_tmp	= $file['tmp_name'];
	$file_size	= $file['size'];
	$file_error	= $file['error'];
	// ekstensi
	$extensi 	= explode('.', $file_name);
	$extensi 	= strtoupper(end($extensi));
	$allowed	= array('PNG', 'JPG', 'JPEG');


	if($kode == "" || $merek == "" || $type == "" || $warna == "" || $harga == ""){
	    ?>
	        <script type="text/javascript">
	            alert("Data tidak boleh kosong !");
	            window.location.href ="?page=motor&action=update_motor&kd_motor=<?= $data['motor_kode']; ?>&foto=<?php echo $data['gambar'] ?>";
	        </script>
	    <?php
	} else {
		if ($file == "") {
			$sql = "UPDATE tbl_motor SET motor_merk='$merek',motor_type='$type',motor_warna_pilihan='$warna',motor_harga='$harga' WHERE motor_kode='$kode'";
			$update = $conn->query($sql) or die(mysqli_error($conn));

			if ($update > 0) {
				?>
					<script type="text/javascript">
						alert('Data berhasil diubah.');
						window.location.href="?page=motor";
					</script>
				<?php
			} else {
				?>
					<script type="text/javascript">
						alert('Data gagal diubah.');
						window.location.href="?page=motor&action=update_motor&kd_motor=<?= $data['motor_kode']; ?>&foto=<?php echo $data['gambar'] ?>";
					</script>
				<?php
			}
			$conn->close();
		} else {
			if (in_array($extensi, $allowed)) {
				if ($file_error === 0) {
					if ($file_size <= 2097152) {
						// unlink foto
						unlink('./motor/img/'.$foto);
						// location and generate name
						$file_name_new = uniqid('', true).'.'.$extensi;
						$path = './motor/img/' . $file_name_new;
						if (move_uploaded_file($file_tmp, $path)) {
							$sql = "UPDATE tbl_motor SET motor_merk='$merek',motor_type='$type',motor_warna_pilihan='$warna', motor_harga='$harga',gambar='$file_name_new' WHERE motor_kode='$kode'";
							$update = $conn->query($sql) or die(mysqli_error($conn));

							if ($update > 0) {
								?>
									<script type="text/javascript">
										alert('Data berhasil diubah.');
										window.location.href="?page=motor";
									</script>
								<?php
							} else {
								?>
									<script type="text/javascript">
										alert('Data gagal diubah.');
										window.location.href="?page=motor";
									</script>
								<?php
							}
							$conn->close();
						}
					} else{
						?>
							<script type="text/javascript">
								alert('Ukuran file terlalu besar !!!');
								window.location.href="?page=motor&action=update_motor&kd_motor=<?= $data['motor_kode']; ?>&foto=<?php echo $data['gambar'] ?>";
							</script>
						<?php
					}
				} else{
					?>
						<script type="text/javascript">
							alert('Error !!!');
							window.location.href="?page=motor&action=update_motor&kd_motor=<?= $data['motor_kode']; ?>&foto=<?php echo $data['gambar'] ?>";
						</script>
					<?php
				}
			}else{
				?>
					<script type="text/javascript">
						alert('Extensi tidak diperbolehkan !!!');
						window.location.href="?page=motor&action=update_motor&kd_motor=<?= $data['motor_kode']; ?>&foto=<?php echo $data['gambar'] ?>";
					</script>
				<?php
			}
		}
	}

}

?>
