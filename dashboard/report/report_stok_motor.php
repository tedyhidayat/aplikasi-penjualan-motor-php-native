<?php


$sql = "SELECT * FROM tbl_motor ORDER BY motor_kode";
$read_motor = $conn->query($sql);
$res = mysqli_num_rows($read_motor);


?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
  </section>

  <!-- Main content -->
  <section class="invoice" style="box-shadow: 0px 0px 10px black;">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-motorcycle"></i> Laporan Data Motor
          <small class="pull-right">Per tanggal: <b><?php echo date('d/m/Y'); ?></b></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-12 invoice-col">
      </div>
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-hover table-striped">
            <tr class="alert-danger" style="border-bottom: 2px solid gray;">
              <th width="100">Kode Motor</th>
              <th>Merek</th>
              <th>Type</th>
              <th>Warna</th>
              <th>Harga</th>
              <th>Gambar</th>
            </tr>

          <?php foreach( $read_motor as $data ) : ?>
            <tr>
              <td><?= htmlspecialchars($data["motor_kode"]); ?></td>
              <td><?= htmlspecialchars($data["motor_merk"]); ?></td>
              <td><?= htmlspecialchars($data["motor_type"]); ?></td>
              <td><?= htmlspecialchars($data["motor_warna_pilihan"]); ?></td>
              <td> Rp. <?= number_format($data['motor_harga'], 2, ',','.'); ?></td>
              <td> <img class="img-thumbnail" width="100px" src="./motor/img/<?= $data['gambar']; ?>" alt="Motor pic"></td>
            </tr>
            <?php endforeach; ?>
          </table>
      </div>
      <!-- /.col -->
    </div>
          <?php
            $sql = "SELECT * FROM tbl_motor";
            $query = $conn->query($sql);
            $jml_data = mysqli_num_rows($query);
          ?>

          <div class="box-footer clearfix">
            <h3><span class="text-left">Jumlah data</span> <span class="label label-warning"><?= $jml_data;?></span></h3>
          </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="#" onclick="print();" target="_blank" class="btn btn-danger pull-right"><i class="fa fa-print"></i> Cetak </a>
      </div>
    </div>
  </section>
  <!-- /.content -->
  <div class="clearfix"></div>
