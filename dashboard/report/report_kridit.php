<?php


$sql = "SELECT * FROM tbl_beli_kridit ORDER BY kridit_kode ASC";
$read_beli_kridit = $conn->query($sql);
$res = mysqli_num_rows($read_beli_kridit);


?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
  </section>

  <!-- Main content -->
  <section class="invoice" style="box-shadow: 0px 0px 10px black;">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-"></i> Laporan Data Penjualan Beli Kridit
          <small class="pull-right">Per tanggal: <b><?php echo date('d/m/Y'); ?></b></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-12 invoice-col">
      </div>
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-hover table-striped">
            <tr style="border-bottom: 2px solid gray; background-color: #B3B3B3;">
              <th width="100">Kode</th>
              <th>No KTP</th>
              <th>Kode Motor</th>
              <th>Tanggal Pembelian</th>
              <th>FC KTP</th>
              <th>FC KK</th>
              <th>FC Slip Gaji</th>
            </tr>

          <?php foreach( $read_beli_kridit as $data ) : ?>
            <tr style="font-size: 12px;">
              <td style="font-weight: bold;"><?= htmlspecialchars_decode($data["kridit_kode"]); ?></td>
              <td><?= htmlspecialchars_decode($data["pembeli_no_ktp"]); ?></td>
              <td><?= htmlspecialchars_decode($data["motor_kode"]); ?></td>
              <td>
                <?php
                  if (!empty($data['kridit_tanggal'])) {
                    $tanggal = substr($data['kridit_tanggal'], 8, 2);
                    $bulan = substr($data['kridit_tanggal'], 5, 2);
                    $tahun = substr($data['kridit_tanggal'], 0, 4);
                    $tanggal_beli = "$tanggal-$bulan-$tahun";
                  }
                ?>
                <?= htmlspecialchars_decode($tanggal_beli); ?>
              </td>
              <td><?= htmlspecialchars_decode($data["fotokopi_ktp"]); ?></td>
              <td><?= htmlspecialchars_decode($data["fotokopi_kk"]); ?></td>
              <td><?= htmlspecialchars_decode($data["fotokopi_slip_gaji"]); ?></td>
            </tr>
            <?php endforeach; ?>
          </table>
      </div>
      <!-- /.col -->
    </div>
          <?php
            $sql = "SELECT * FROM tbl_beli_kridit";
            $query = $conn->query($sql);
            $jml_data = mysqli_num_rows($query);
          ?>

          <div class="box-footer clearfix">
            <h3><span class="text-left">Jumlah data</span> <span class="label label-warning"><?= $jml_data;?></span></h3>
          </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="#" onclick="print();" target="_blank" class="btn btn-danger pull-right"><i class="fa fa-print"></i> Cetak </a>
      </div>
    </div>
  </section>
  <!-- /.content -->
  <div class="clearfix"></div>
