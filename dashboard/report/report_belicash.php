<?php


$sql = "SELECT * FROM tbl_beli_cash ORDER BY cash_kode ASC";
$read_beli_cash = $conn->query($sql);
$res = mysqli_num_rows($read_beli_cash);


?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
  </section>

  <!-- Main content -->
  <section class="invoice" style="box-shadow: 0px 0px 10px black;">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-"></i> Laporan Data Penjualan Beli Cash
          <small class="pull-right">Per tanggal: <b><?php echo date('d/m/Y'); ?></b></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-12 invoice-col">
      </div>
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-hover table-striped">
            <tr style="border-bottom: 2px solid gray; background-color: #B3B3B3;">
              <th width="100">Kode</th>
              <th>No KTP</th>
              <th>Kode Motor</th>
              <th>Tanggal Pembelian</th>
              <th>Uang Cash</th>
            </tr>

          <?php foreach( $read_beli_cash as $data ) : ?>
            <tr style="font-size: 12px;">
              <td style="font-weight: bold;"><?= htmlspecialchars_decode($data["cash_kode"]); ?></td>
              <td><?= htmlspecialchars_decode($data["pembeli_no_ktp"]); ?></td>
              <td><?= htmlspecialchars_decode($data["motor_kode"]); ?></td>
              <td>
                <?php
                  if (!empty($data['cash_tanggal'])) {
                    $tanggal = substr($data['cash_tanggal'], 8, 2);
                    $bulan = substr($data['cash_tanggal'], 5, 2);
                    $tahun = substr($data['cash_tanggal'], 0, 4);
                    $tanggal_beli = "$tanggal-$bulan-$tahun";
                  }
                ?>
                <?= htmlspecialchars_decode($tanggal_beli); ?>
              </td>
              <td>Rp. <?= number_format($data["cash_bayar"], 2, ',','.'); ?></td>
            </tr>
            <?php endforeach; ?>
          </table>
      </div>
      <!-- /.col -->
    </div>
          <?php
            $sql = "SELECT * FROM tbl_beli_cash";
            $query = $conn->query($sql);
            $jml_data = mysqli_num_rows($query);
          ?>

          <div class="box-footer clearfix">
            <h3><span class="text-left">Jumlah data</span> <span class="label label-warning"><?= $jml_data;?></span></h3>
          </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="#" onclick="print();" target="_blank" class="btn btn-danger pull-right"><i class="fa fa-print"></i> Cetak </a>
      </div>
    </div>
  </section>
  <!-- /.content -->
  <div class="clearfix"></div>
