<?php
  $sql = "SELECT * FROM tbl_pembeli";
  $query = $conn->query($sql);
  $jml_pembeli = mysqli_num_rows($query);

  $sql = "SELECT * FROM tbl_motor";
  $query = $conn->query($sql);
  $jml_motor = mysqli_num_rows($query);

?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="callout callout-info">
    <h4>Selamat Datang di Halaman Administrator</h4>

    <p>Pilih Menu untuk mengatur aplikasi anda.</p>
  </div>

  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3><?= $jml_pembeli; ?><sup style="font-size: 15px"> Pembeli</sup></h3>

          <p>Data Pembeli</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <a href="?page=pembeli" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?= $jml_motor; ?><sup style="font-size: 15px"> Unit</sup></h3>

          <p>Data Motor</p>
        </div>
        <div class="icon">
          <i class="fa fa-motorcycle"></i>
        </div>
        <a href="?page=motor" class="small-box-footer">
          More info <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>
  </div>
  <!-- /.row -->

  <!-- <div class="box">
    <div class="box-header">
      <h3 class="box-title">Simple Full Width Table</h3>

      <div class="box-tools">
        <ul class="pagination pagination-sm no-margin pull-right">
          <li><a href="#">&laquo;</a></li>
          <li><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">&raquo;</a></li>
        </ul>
      </div>
    </div> -->
    <!-- /.box-header -->
   <!--  <div class="box-body no-padding">
      <table class="table">
        <tr>
          <th style="width: 10px">#</th>
          <th>Task</th>
          <th>Progress</th>
          <th style="width: 40px">Label</th>
        </tr>
        <tr>
          <td>1.</td>
          <td>Update software</td>
          <td>
            <div class="progress progress-xs">
              <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
            </div>
          </td>
          <td><span class="badge bg-red">55%</span></td>
        </tr>
        <tr>
          <td>2.</td>
          <td>Clean database</td>
          <td>
            <div class="progress progress-xs">
              <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
            </div>
          </td>
          <td><span class="badge bg-yellow">70%</span></td>
        </tr>
        <tr>
          <td>3.</td>
          <td>Cron job running</td>
          <td>
            <div class="progress progress-xs progress-striped active">
              <div class="progress-bar progress-bar-primary" style="width: 30%"></div>
            </div>
          </td>
          <td><span class="badge bg-light-blue">30%</span></td>
        </tr>
        <tr>
          <td>4.</td>
          <td>Fix and squish bugs</td>
          <td>
            <div class="progress progress-xs progress-striped active">
              <div class="progress-bar progress-bar-success" style="width: 90%"></div>
            </div>
          </td>
          <td><span class="badge bg-green">90%</span></td>
        </tr>
      </table>
    </div> -->
    <!-- /.box-body -->
  <!-- </div> -->
  <!-- /.box -->
</section>