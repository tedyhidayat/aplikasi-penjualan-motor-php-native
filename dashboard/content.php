<?php
$page = @$_GET['page'];
$action = @$_GET['action'];

    if($page == "pembeli"){
        if($action == ""){
            include "pembeli/read_pembeli.php";
        }
        else if ($action == "create_pembeli"){
            include "pembeli/create_pembeli.php";
        }
        else if ($action == "update_pembeli"){
            include "pembeli/update_pembeli.php";
        }
        else if ($action == "delete_pembeli"){
            include "pembeli/delete_pembeli.php";
        }
    } else if($page == "motor"){
        if($action == ""){
            include "motor/read_motor.php";
        }
        else if ($action == "create_motor"){
            include "motor/create_motor.php";
        }
        else if ($action == "update_motor"){
            include "motor/update_motor.php";
        }
        else if ($action == "delete_motor"){
            include "motor/delete_motor.php";
        }
    } else if($page == "beli_cash"){
        if($action == ""){
            include "penjualan/beli_cash/read_belicash.php";
        }
        else if ($action == "create_belicash"){
            include "penjualan/beli_cash/create_belicash.php";
        }
        else if ($action == "update_belicash"){
            include "penjualan/beli_cash/update_belicash.php";
        }
        else if ($action == "delete_belicash"){
            include "penjualan/beli_cash/delete_belicash.php";
        }
        else if ($action == "print_belicash"){
            include "penjualan/beli_cash/print_belicash.php";
        }
        else if ($action == "print_proses_belicash"){
            include "penjualan/beli_cash/print_proses_belicash.php";
        }
    } else if($page == "beli_kridit"){
        if($action == ""){
            include "penjualan/beli_kridit/read_belikridit.php";
        }
        else if ($action == "create_belikridit"){
            include "penjualan/beli_kridit/create_belikridit.php";
        }
        else if ($action == "update_belikridit"){
            include "penjualan/beli_kridit/update_belikridit.php";
        }
        else if ($action == "delete_belikridit"){
            include "penjualan/beli_kridit/delete_belikridit.php";
        }
        else if ($action == "print_belikridit"){
            include "penjualan/beli_kridit/print_belikridit.php";
        }
    } else if($page == "penjualan"){
        if($action == ""){
            include "penjualan/penjualan.php";
        }
    }else if($page == "report"){
        if($action == "motor"){
            include "report/report_stok_motor.php";
        }
        if($action == "belicash"){
            include "report/report_belicash.php";
        }
        if($action == "belikridit"){
            include "report/report_kridit.php";
        }
    } else if ($page == ""){
        include "../dashboard/content_dashboard.php";
    } else {
        echo "404 ! Halaman tidak ditemukan.";
    }
?>