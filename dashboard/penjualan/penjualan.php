

<!-- Content Header (page header) -->
<!-- <section class="content-header">
  <h1>
    Penjualan
    <small><?= date('D, d/m/Y');?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Penjualan</li>
  </ol>
</section> -->

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <a href="#" class="btn btn-success btn-block margin-bottom" data-toggle="modal" data-target="#transaksi"> <i class="fa fa-cart-plus"></i> Transaksi</a>

     <!--  <div class="box box-solid">
        <div class="box-header with-border" style="background-color: #00ABC4;">
          <h3 class="box-title" style="color: white;"><b>Menu</b></h3>

          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus" style="color: white;"></i>
            </button>
          </div>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="#" data-toggle="modal" data-target="#paket_kridit"><i class="fa fa-inbox"></i> <u>Tambah Paket Kridit</u></a></li>
            <li><a href="?page=cicilan"><i class="fa fa-file-text-o"></i> <u>Daftar Cicilan Pelanggan</u></a></li>
          </ul>
        </div> -->
        <!-- /.box-body -->
      <!-- </div> -->
    </div>
    <!-- /.col -->
    <div class="col-md-12">

      <!-- data beli cash -->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-"></i> Data Pembelian Cash</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php include 'beli_cash/read_belicash.php'; ?>
        </div>
        <!-- /.box-body -->
      </div>

      <!-- data beli kridit -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-"></i> Data Pembelian Kridit</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php include 'beli_kridit/read_belikridit.php'; ?>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->



<div class="modal fade" id="transaksi">
  <div class="modal-dialog modal-sm bg-danger">
    <div class="modal-content">
      <div class="modal-body text-center">
        <a href="#" data-toggle="modal" data-target="#beli_cash" class="btn btn-danger">Beli Cash</a>
        <a href="#" data-toggle="modal" data-target="#beli_kridit" class="btn btn-info">Beli Kridit</a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="beli_cash">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <?php include 'beli_cash/create_belicash.php'; ?>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="beli_kridit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-list-ul"></i> Menu transaksi</h4>
      </div>
      <div class="modal-body">
        <?php include 'beli_kridit/create_belikridit.php'; ?>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="paket_kridit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-file-text-o"></i> Paket Kridit</h4>
      </div>
      <div class="modal-body">
        <?php include 'paket_kridit/create_pkredit.php'; ?>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>