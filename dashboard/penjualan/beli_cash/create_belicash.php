<?php
  $sql		= "SELECT MAX(cash_kode) from tbl_beli_cash";
  $carikode = $conn->query($sql);
  $datakode = mysqli_fetch_array($carikode);
  if($datakode){
  	$tanggal = date('dmY');
    $nilaikode = substr($datakode[0], 15);
    $kode = (int) $nilaikode;
    $kode = $kode + 1;
    $hasilkode = "CASH/". $tanggal . "/" . str_pad($kode, 5, "0", STR_PAD_LEFT);
  }
  else{
    $hasilkode = "CASH/".$tanggal."/00001";
  }

  $query = "SELECT * FROM tbl_pembeli ORDER BY pembeli_no_ktp";
  $read_pembeli = $conn->query($query);

  $query = "SELECT * FROM tbl_motor ORDER BY motor_kode";
  $read_motor = $conn->query($query);
?>



<section class="content-header text-center">
  <h1>
    Pembelian Cash
    <small><?= date('D, d/m/Y');?></small>
  </h1>
</section>
<section class="content">
	<!-- general form elements disabled -->
	<div class="box box-success">
	  <!-- /.box-header -->
	  <div class="box-body">
	  	<div class="col-md-10 col-md-offset-1"  style="padding-bottom: 30px;">
		    <form role="form" method="post" action="">
		      <div class="form-group has-warning">
		        <label for="kode">Kode</label>
		        <input type="text" name="cash_kode" class="form-control" id="kode" value="<?= $hasilkode; ?>" autocomplete="off" readonly>
		      </div>
		      <div class="form-group">
		        <label for="ds">No KTP Pembeli</label>
		        <select name="no_ktp" class="form-control">
		        	<option value=""> -- PILIH NOMOR KTP -- </option>
		        	<?php foreach( $read_pembeli as $data ):?>
		        		<?php if ($data['pembeli_no_ktp']): ?>
		        			<option value="<?= $data['pembeli_no_ktp']; ?>"> <?= $data['pembeli_nama']; ?> </option>
		        		<?php else: ?>
		        			<option value=""> -- PILIH NOMOR KTP -- </option>
		        		<?php endif; ?>
		        	<?php endforeach; ?>
		        </select>
		      </div>
		      <div class="form-group">
     		    <label for="ds">Merk Motor</label>
     		    <select name="kode_motor" class="form-control" id="merk_motor" onchange="changeValue(this.value)">
     		    	<option>-- PILIH DATA MOTOR --</option>
     		    	<?php
     		    	    $res = mysqli_query($conn, "select * from tbl_motor");
     		    	    $arrayjs = "var dtmotor = new Array();\n";
     		    	    while ($data = mysqli_fetch_array($res)) {
     		    	        echo '<option value="' . $data['motor_kode'] . '">' . $data['motor_merk'] . '</option>';
     		    	        $arrayjs .= "dtmotor['" . $data['motor_kode'] . "'] = {motor_harga:'" . addslashes($data['motor_harga']) . "'};\n";
     		    	    }
     		    	?>
     		    </select>
			  <script type="text/javascript">
			  	<?php echo $arrayjs; ?>
			  		function changeValue(merk_motor){
			  		   	document.getElementById('motor_harga').value = dtmotor[merk_motor].motor_harga;
			  		};
			  </script>
     		  </div>
     		  <div class="form-group">
     		  	<input type="number" class="form-control" id="motor_harga" readonly value="0">
     		  </div>
		      <div class="form-group">
		        <label for="uang_cash">Nominal Uang Cash</label>
		        <input type="number" name="uang_cash" class="form-control" id="uang_cash" placeholder="Masukan Nominal" autocomplete="off">
		      </div>
		</div>
	  </div>
	  <!-- /.box-body -->
	  <!-- .box-footer -->
	  <div class="box-footer text-right">
	  	<div class="col-md-10 col-md-offset-1">
	  		<button type="reset" name="reset" class="btn btn-danger text-left" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i> Cancel</button>
	    	<button type="submit" name="tambah" class="btn btn-success text-right"><i class="fa fa-plus"></i> Simpan</button>
	    	</form>
	    </div>
	  </div>
	</div>
	<!-- /.box -->
</section>

<?php

if (isset($_POST['tambah'])) {

	$cash_kode	= htmlspecialchars($_POST['cash_kode']);
	$no_ktp 	= htmlspecialchars($_POST['no_ktp']);
	$kode_motor = htmlspecialchars($_POST['kode_motor']);
	$uang_cash 	= htmlspecialchars($_POST['uang_cash']);
	$date 		= date('Ymd');

	if ($cash_kode == "" || $no_ktp == "" || $kode_motor == "" || $uang_cash == "") {
		?>
			<script type="text/javascript">
				alert('Data tidak boleh kosong !');
				window.location.href="?page=penjualan";
			</script>
		<?php
	} else {
		$sql = "INSERT INTO tbl_beli_cash VALUES ('$cash_kode','$no_ktp','$kode_motor','$date','$uang_cash')";
		$insert = $conn->query($sql);

		if ($insert > 0) {
			?>
				<script type="text/javascript">
					alert('Data berhasil ditambahkan.');
					window.location.href="?page=penjualan";
				</script>
			<?php
		} else {
			echo "Data gagal tersimpan !";
		}

		$conn->close();
	}
}

?>
