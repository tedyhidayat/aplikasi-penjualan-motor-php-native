<?php
    $id		= $_GET['cash_kode'];
    $sql	= $conn->query("SELECT * FROM tbl_beli_cash WHERE cash_kode ='$id'") or die (mysqli_error());
    $data 	= mysqli_fetch_array($sql);
?>

<?php
	$no_ktp	= $_GET['no_ktp'];
	$sql 	= "SELECT * FROM tbl_pembeli ORDER BY pembeli_no_ktp='$no_ktp'";
	$read_pembeli = $conn->query($sql);
?>

<?php
	$kode_motor	= $_GET['kode_motor'];
	$sql 	= "SELECT * FROM tbl_motor ORDER BY motor_kode='$kode_motor'";
	$read_motor = $conn->query($sql);
?>


<section class="content-header text-center">
  <h1>
    Edit Data Pembelian Cash
    <small><?= date('D, d/m/Y');?></small>
  </h1>
</section>
<section class="content">
	<!-- general form elements disabled -->
	<div class="box box-success">
	  <!-- /.box-header -->
	  <div class="box-body">
	  	<div class="col-md-10 col-md-offset-1"  style="padding-bottom: 30px;">
		    <form role="form" method="post" action="">
		      <div class="form-group has-warning">
		        <label for="kode">Kode</label>
		        <input type="text" name="cash_kode" class="form-control" id="kode" value="<?= $data['cash_kode']; ?>" autocomplete="off" readonly>
		      </div>
		      <div class="form-group">
		        <label for="ds">No KTP Pembeli</label>
		        <select name="no_ktp" class="form-control">
		        	<option value="<?= $no_ktp; ?>"> <?= $no_ktp; ?> </option>
		        	<?php foreach( $read_pembeli as $data ):?>
		        		<?php if ($data['pembeli_no_ktp']): ?>
		        			<option value="<?= $data['pembeli_no_ktp']; ?>"> <?= $data['pembeli_no_ktp']; ?> </option>
		        		<?php else: ?>
		        			<option value=""> -- PILIH NOMOR KTP -- </option>
		        		<?php endif; ?>
		        	<?php endforeach; ?>
		        </select>
		      </div>
		      <div class="form-group">
		        <label for="ds">Kode Motor</label>
		        <select name="kode_motor" class="form-control">
		        	<option value="<?= $kode_motor; ?>"> <?= $kode_motor; ?> </option>
		        	<?php foreach( $read_motor as $data ):?>
		        		<?php if ($data['motor_kode']): ?>
		        			<option value="<?= $data['motor_kode']; ?>"> <?= $data['motor_kode']; ?> </option>
		        		<?php else: ?>
		        			<option value=""> -- PILIH KODE MOTOR -- </option>
		        		<?php endif; ?>
		        	<?php endforeach; ?>
		        </select>
		      </div>

		      <?php
		          $id		= $_GET['cash_kode'];
		          $sql	= $conn->query("SELECT * FROM tbl_beli_cash WHERE cash_kode ='$id'") or die (mysqli_error());
		          $data 	= mysqli_fetch_array($sql);
		      ?>
		      <div class="form-group">
		        <label for="uang_cash">Nominal Uang Cash</label>
		        <input type="number" name="uang_cash" class="form-control" id="uang_cash" value="<?= $data['cash_bayar']; ?>" autocomplete="off">
		      </div>
		</div>
	  </div>
	  <!-- /.box-body -->
	  <!-- .box-footer -->
	  <div class="box-footer text-right">
	  	<div class="col-md-10 col-md-offset-1">
	  		<button type="reset" name="reset" class="btn btn-danger text-left" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i> Cancel</button>
	    	<button type="submit" name="edit" class="btn btn-success text-right"><i class="fa fa-plus"></i> Simpan</button>
	    	</form>
	    </div>
	  </div>
	</div>
	<!-- /.box -->
</section>

<?php

if (isset($_POST['edit'])) {

	$cash_kode	= htmlspecialchars($_POST['cash_kode']);
	$no_ktp 	= htmlspecialchars($_POST['no_ktp']);
	$kode_motor = htmlspecialchars($_POST['kode_motor']);
	$uang_cash 	= htmlspecialchars($_POST['uang_cash']);

	if ($cash_kode == "" || $no_ktp == "" || $kode_motor == "" || $uang_cash == "") {
		?>
			<script type="text/javascript">
				alert('Data tidak boleh kosong !');
				window.location.href="?page=penjualan";
			</script>
		<?php
	} else {
		$sql = "UPDATE tbl_beli_cash SET pembeli_no_ktp = '$no_ktp', motor_kode = '$kode_motor', cash_bayar = '$uang_cash' WHERE cash_kode ='$cash_kode'";
		$insert = $conn->query($sql);

		if ($insert > 0) {
			?>
				<script type="text/javascript">
					alert('Data berhasil diubah');
					window.location.href="?page=penjualan";
				</script>
			<?php
		} else {
			echo "Data gagal tersimpan !";
		}

		$conn->close();
	}
}

?>
