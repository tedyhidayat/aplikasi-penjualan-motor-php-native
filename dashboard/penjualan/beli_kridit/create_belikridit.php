<?php
  $sql		= "SELECT MAX(kridit_kode) from tbl_beli_kridit";
  $carikode = $conn->query($sql);
  $datakode = mysqli_fetch_array($carikode);
  if($datakode){
  	$tanggal = date('dmY');
    $nilaikode = substr($datakode[0], 18);
    $kode = (int) $nilaikode;
    $kode = $kode + 1;
    $hasilkode = "KRIDIT/". $tanggal . str_pad($kode, 5, "0", STR_PAD_LEFT);
  }
  else{
    $hasilkode = "KRIDIT/".$tanggal."00001";
  }
?>


<section class="content-header text-center">
  <h1>
    Pembelian kridit
    <small><?= date('D, d/m/Y');?></small>
  </h1>
</section>
<section class="content">
	<!-- general form elements disabled -->
	<div class="box box-success">
	  <!-- /.box-header -->
	  <div class="box-body">
	  	<div class="col-md-10 col-md-offset-1" style="padding-bottom: 30px;">
		    <form role="form" method="post" action="">
		      <div class="form-group has-warning">
		        <label for="kode">Kode</label>
		        <input type="text" name="kridit_kode" class="form-control" id="kode" value="<?= $hasilkode; ?>" autocomplete="off" readonly>
		      </div>
		      <div class="form-group">
		        <label for="ds">Nama Pembeli</label>
		        <select name="no_ktp" class="form-control" id="pembeli"">
		        	<option selected="selected">-- PILIH DATA PEMBELI --</option>
		        	<?php
		        	    $result = mysqli_query($conn, "select * from tbl_pembeli");
		        	    while ($row_s = mysqli_fetch_array($result)) {
		        	        echo '<option value="' . $row_s['pembeli_no_ktp'] . '">' . $row_s['pembeli_nama'] . '</option>';
		        	    }
		        	?>
		        </select>
		      </div>
<!--  -->
     		  <div class="form-group">
     		    <label for="ds">Merk Motor</label>
     		    <select name="kode_motor" class="form-control" id="motor_merk" onchange="changeValue(this.value)">
     		    	<option>-- PILIH DATA MOTOR --</option>
     		    	<?php
     		    	    $res = mysqli_query($conn, "select * from tbl_motor");
     		    	    $arrayjs = "var dtmotor = new Array();\n";
     		    	    while ($data = mysqli_fetch_array($res)) {
     		    	        echo '<option value="' . $data['motor_kode'] . '">' . $data['motor_merk'] . '</option>';
     		    	        $arrayjs .= "dtmotor['" . $data['motor_kode'] . "'] = {motor_kode:'" . addslashes($data['motor_harga']) . "'};\n";
     		    	    }
     		    	?>
     		    </select>
			  <script type="text/javascript">
			  	<?php echo $arrayjs; ?>
			  		function changeValue(motor_merk){
			  		   	document.getElementById('motor_kode').value = dtmotor[motor_merk].motor_kode;
			  		};
			  </script>
     		  </div>
     		  <div class="form-group">
     		  	<input type="text" class="form-control" id="motor_kode" step="any" readonly value="0">
     		  </div>
<!--  -->
			 <div class="form-group">
			   <select name="jumlah_cicilan" class="form-control" id="jumlah_cicilan" step="any">
     		    	<option>-- Pilih Rentang cicilan --</option>
     		    	<option value="12"> 12 Bulan </option>
     		    	<option value="24"> 24 Bulan </option>
     		    	<option value="36"> 36 Bulan </option>
     		    </select>
     		 </div>
		      <!-- radio -->
              <!-- checkbox -->
              <div class="form-group">
                <label>
                	Foto Copy KTP
                </label>
                <input type="checkbox" name="fc_ktp" value="1" class="flat-green">
              </div>
              <div class="form-group">
                <label>
                	Foto Copy KK
                </label>
                <input type="checkbox" name="fc_kk" value="1" class="flat-green">
              </div>
              <div class="form-group">
              	 <label>
                	Foto Copy Slip Gaji
                </label>
                <input type="checkbox" name="fc_gaji" value="1" class="flat-green">
              </div>
		</div>
	  </div>
	  <!-- /.box-body -->
	  <!-- .box-footer -->
	  <div class="box-footer text-right">
	  	<div class="col-md-10 col-md-offset-1">
	  		<button type="reset" name="reset" class="btn btn-danger text-left" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i> Cancel</button>
	    	<button type="submit" name="tambah" class="btn btn-success text-right"><i class="fa fa-plus"></i> Simpan</button>
	    	</form>
	    </div>
	  </div>
	</div>
	<!-- /.box -->
</section>

<?php

if (isset($_POST['tambah'])) {

	$kridit_kode = $_POST['kridit_kode'];
	$no_ktp = $_POST['no_ktp'];
	$kode_motor = $_POST['kode_motor'];
	$fc_ktp = $_POST['fc_ktp'];
	$fc_kk = $_POST['fc_kk'];
	$fc_gaji = $_POST['fc_gaji'];
	$jumlah_cicilan = $_POST['jumlah_cicilan'];
	$date = date('Ymd');

	if ($kridit_kode == "" || $no_ktp == "" || $kode_motor == "") {
		?>
			<script type="text/javascript">
				alert('Data tidak boleh kosong !');
				window.location.href="?page=penjualan";
			</script>
		<?php
	} else {
		$sql = "INSERT INTO tbl_beli_kridit VALUES ('$kridit_kode','$no_ktp','$kode_motor','$date','$fc_ktp','$fc_kk','$fc_gaji','$jumlah_cicilan')";
		$insert = $conn->query($sql);

		if ($insert > 0) {
			?>
				<script type="text/javascript">
					alert('Data berhasil ditambahkan.');
					window.location.href="?page=penjualan";
				</script>
			<?php
		} else {
			echo "Data gagal tersimpan !";
		}

		$conn->close();
	}
}

?>

<!-- jQuery 3 -->
<!-- <script src="../_assets/_assets/bower_components/jquery/dist/jquery.min.js"></script> -->