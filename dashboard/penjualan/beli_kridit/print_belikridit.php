<?php

$id = $_GET['kridit_kode'];
$query = "SELECT * FROM tbl_beli_kridit WHERE kridit_kode = '$id'";
$sql = $conn->query($query);
$data = mysqli_fetch_array($sql);

if (!empty($data['kridit_tanggal'])) {
  $tanggal = substr($data['kridit_tanggal'], 8, 2);
  $bulan = substr($data['kridit_tanggal'], 5, 2);
  $tahun = substr($data['kridit_tanggal'], 0, 4);
  $tanggal_beli = "$tanggal-$bulan-$tahun";
}

$sql = "SELECT * FROM tbl_pembeli WHERE pembeli_no_ktp = '$data[pembeli_no_ktp]'";
$read_pembeli = $conn->query($sql);
$res = mysqli_fetch_array($read_pembeli);

$sql = "SELECT * FROM tbl_motor WHERE motor_kode = '$data[motor_kode]'";
$read_motor = $conn->query($sql);
$res_motor = mysqli_fetch_array($read_motor);


    if ($_SESSION['Administrator']){
        $hak = $_SESSION['Administrator'];
    }
    $query = "SELECT * FROM tbl_akun WHERE id = '$hak'";
    $sql_user = $conn->query($query);
    $data_user = mysqli_fetch_array($sql_user);



?>

  <!-- Content Header (Page header) -->
  <section class="content-header">
  </section>

  <!-- Main content -->
  <section class="invoice" style="box-shadow: 0px 0px 10px black;">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> INVOICE
          <small class="pull-right">Date: <b><?php echo date('l, d/m/Y'); ?></b></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Dari
        <p>
        <address>
          <strong>PT. Adila Motor Group</strong><br>
          JL. Bantargebang-Setu<br>
          KOTA BEKASI 17156<br>
          <br>
          Telepon: (021) 123-5432<br>
          Email: info@adilagroup.com
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Kepada
        <p>
        <address>
          <strong><?= $res['pembeli_nama']; ?></strong><br>
          No. KTP : <?= $res['pembeli_no_ktp']; ?><br>
          Nomor HP : <?= $res['pembeli_hp']; ?><br><br>
          Alamat : <?= htmlspecialchars_decode($res['pembeli_alamat']); ?><br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        ID Petugas : <b><?= $data_user['id']; ?></b><br>
        Nama Petugas : <b><?= $data_user['nama']; ?></b><br>
        Tanggal Beli : <b><?= $tanggal_beli; ?></b><br><br><br>
        Kode Beli kridit :<br> <b style="font-size: 25px;"><mark style="background-color: yellow;"><?= $data['kridit_kode']; ?></mark></b><br>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr class="bg-primary">
            <th>Kode Motor</th>
            <th>Merek</th>
            <th>Warna</th>
            <th>Cicilan</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td width="200"><b><?= $data['motor_kode']; ?></b></td>
            <td><b><?= $res_motor['motor_merk']; ?></b></td>
            <td><b><?= $res_motor['motor_warna_pilihan']; ?></b></td>
            <td > <b><?= $data["jumlah_cicilan"]; ?> Bulan</b></td>
          </tr>
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="#" onclick="print();" target="_blank" class="btn btn-danger pull-right"><i class="fa fa-print"></i> Cetak Invoice</a>
      </div>
    </div>
  </section>
  <!-- /.content -->
  <div class="clearfix"></div>
