<?php
    $id		= $_GET['kridit_kode'];
    $sql	= $conn->query("SELECT * FROM tbl_beli_kridit WHERE kridit_kode ='$id'") or die (mysqli_error());
    $data 	= mysqli_fetch_array($sql);
?>
<?php
	$no_ktp	= $_GET['no_ktp'];
	$sql 	= "SELECT * FROM tbl_pembeli ORDER BY pembeli_no_ktp='$no_ktp'";
	$read_pembeli = $conn->query($sql);
?>


<section class="content-header text-center">
  <h1>
    Edit Data Pembelian Kridit
    <small><?= date('D, d/m/Y');?></small>
  </h1>
</section>
<section class="content">
	<!-- general form elements disabled -->
	<div class="box box-success">
	  <!-- /.box-header -->
	  <div class="box-body">
	  	<div class="col-md-10 col-md-offset-1" style="padding-bottom: 30px;">
		    <form role="form" method="post" action="">
		      <div class="form-group has-warning">
		        <label for="kode">Kode</label>
		        <input type="text" name="kridit_kode" class="form-control" id="kode" value="<?= $data['kridit_kode']; ?>" autocomplete="off" readonly>
		      </div>
		      <div class="form-group">
		        <label for="ds">No KTP Pembeli</label>
		        <select name="no_ktp" class="form-control" id="pembeli"">
		        	<option value="<?= $data['pembeli_no_ktp']; ?>"> <?= $data['pembeli_no_ktp']; ?> </option>
		        	<?php foreach( $read_pembeli as $data ):?>
		        		<?php if ($data['pembeli_no_ktp']): ?>
		        			<option value="<?= $data['pembeli_no_ktp']; ?>"> <?= $data['pembeli_nama']; ?> </option>
		        		<?php else: ?>
		        			<option value=""> -- PILIH NOMOR KTP -- </option>
		        		<?php endif; ?>
		        	<?php endforeach; ?>
		        </select>
		      </div>
<!--  -->
     		  <div class="form-group">
     		    <label for="ds">Kode Motor</label>
     		    <?php
     		        $id		= $_GET['kridit_kode'];
     		        $sql	= $conn->query("SELECT * FROM tbl_beli_kridit WHERE kridit_kode ='$id'") or die (mysqli_error());
     		        $data 	= mysqli_fetch_array($sql);
     		    ?>
     		    <select name="kode_motor" class="form-control" id="motor_merk" onchange="changeValue(this.value)">
     		    	<option value="<?= $data_mo['motor_kode']; ?>"> <?= $data['motor_kode']; ?> </option>
     		    	<?php
     		    	    $res = mysqli_query($conn, "select * from tbl_motor");
     		    	    $arrayjs = "var dtmotor = new Array();\n";
     		    	    while ($data = mysqli_fetch_array($res)) {
     		    	        echo '<option value="' . $data['motor_kode'] . '">' . $data['motor_merk'] . '</option>';
     		    	        $arrayjs .= "dtmotor['" . $data['motor_kode'] . "'] = {motor_kode:'" . addslashes($data['motor_harga']) . "'};\n";
     		    	    }
     		    	?>
     		    </select>
			  <script type="text/javascript">
			  	<?php echo $arrayjs; ?>
			  		function changeValue(motor_merk){
			  		   	document.getElementById('motor_kode').value = dtmotor[motor_merk].motor_kode;
			  		};
			  </script>
     		  </div>
     		  <div class="form-group">
     		  	<input type="text" class="form-control" id="motor_kode" step="any" readonly value="">
     		  </div>
<!--  -->
			 <div class="form-group">
			 	<?php
			 	    $id		= $_GET['kridit_kode'];
			 	    $sql	= $conn->query("SELECT * FROM tbl_beli_kridit WHERE kridit_kode ='$id'") or die (mysqli_error());
			 	    $data 	= mysqli_fetch_array($sql);
			 	?>
			   <select name="jumlah_cicilan" class="form-control" id="jumlah_cicilan" step="any">
     		    	<option value="<?= $data['jumlah_cicilan']; ?>"><?= $data['jumlah_cicilan']; ?> Bulan</option>
     		    	<option value="12"> 12 Bulan </option>
     		    	<option value="24"> 24 Bulan </option>
     		    	<option value="36"> 36 Bulan </option>
     		    </select>
     		 </div>
		</div>
	  </div>
	  <!-- /.box-body -->
	  <!-- .box-footer -->
	  <div class="box-footer text-right">
	  	<div class="col-md-10 col-md-offset-1">
	  		<button type="reset" name="reset" class="btn btn-danger text-left" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i> Cancel</button>
	    	<button type="submit" name="edit" class="btn btn-success text-right"><i class="fa"></i> Simpan</button>
	    	</form>
	    </div>
	  </div>
	</div>
	<!-- /.box -->
</section>


<?php

if (isset($_POST['edit'])) {

	$kridit_kode = $_GET['kridit_kode'];
	$no_ktp = $_POST['no_ktp'];
	$kode_motor = $_POST['kode_motor'];
	$jumlah_cicilan = $_POST['jumlah_cicilan'];

	if ($kridit_kode == "" || $no_ktp == "" || $kode_motor == "") {
		?>
			<script type="text/javascript">
				alert('Data tidak boleh kosong !');
				window.location.href="?page=beli_kridit&action=update_belikridit&kridit_kode=<?= $data['kridit_kode']; ?>&no_ktp=<?= $data['pembeli_no_ktp']; ?>&kode_motor=<?= $data['motor_kode']; ?>";
			</script>
		<?php
	} else {
		$sql = "UPDATE tbl_beli_kridit SET pembeli_no_ktp = '$no_ktp', motor_kode = '$kode_motor', jumlah_cicilan = '$jumlah_cicilan' WHERE kridit_kode ='$kridit_kode'";
		$edit = $conn->query($sql) or die(mysqli_error($conn));

		if ($edit > 0) {
			?>
				<script type="text/javascript">
					alert('Data berhasil diubah');
					window.location.href="?page=penjualan";
				</script>
			<?php
		} else {
			?>
				<script type="text/javascript">
					alert('Data gagal diubah');
					window.location.href="";
				</script>
			<?php
		}

		$conn->close();
	}
}

?>
