<?php

// batas pagination
$batas = 10;
$halaman = @$_GET['halaman'];

if (empty($halaman)) {
	$posisi = 0;
	$halaman = 1;
} else {
	$posisi = ($halaman - 1) * $batas;
}

// var search

if (isset($_POST['search'])) {
	$keyword = $_POST['table_search'];
	$sql = "SELECT * FROM tbl_beli_kridit WHERE pembeli_no_ktp LIKE '%$keyword%' or kridit_kode LIKE '%$keyword%' or motor_kode LIKE '%$keyword%' ORDER BY kridit_kode ASC LIMIT $posisi,$batas";
	$read_beli_kridit = $conn->query($sql);
	$res = mysqli_num_rows($read_beli_kridit);
} else {
	$sql = "SELECT * FROM tbl_beli_kridit ORDER BY kridit_kode ASC LIMIT $posisi,$batas";
	$read_beli_kridit = $conn->query($sql);
	$res = mysqli_num_rows($read_beli_kridit);
}

?>


	<div class="row">
	  <div class="col-xs-12">
	  	  <div style="margin-bottom: 10px;">
			  <form action="" method="post">
			        <div class="input-group input-group-sm" style="width: 100%;">
			          <input type="text" name="table_search" class="form-control pull-right" placeholder="Masukan keyword untuk mencari data" autocomplete="off">
			          <div class="input-group-btn">
			            <button style="width: 70px;" type="submit" name="search" class="btn btn-primary"><i class="fa fa-search"></i></button>
			          </div>
			        </div>
			  </form>
		  </div>
	      <!-- /.box-header -->
	      <div class="box-body table-responsive no-padding">
	        <table class="table table-hover table-striped">
	          <tr style="border-bottom: 2px solid gray; background-color: #B3B3B3;">
	            <th width="100">Kode</th>
	            <th>No KTP</th>
	            <th>Kode Motor</th>
	            <th>Tanggal Pembelian</th>
	            <th>FC KTP</th>
	            <th>FC KK</th>
	            <th>FC Slip Gaji</th>
	            <th>Cicilan</th>
	            <th class="text-center">Opsi</th>
	          </tr>

		      <?php foreach( $read_beli_kridit as $data ) : ?>
	          <tr style="font-size: 12px;">
	            <td style="font-weight: bold;"><?= htmlspecialchars_decode($data["kridit_kode"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["pembeli_no_ktp"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["motor_kode"]); ?></td>
	            <td>
	            	<?php
	            		if (!empty($data['kridit_tanggal'])) {
	            			$tanggal = substr($data['kridit_tanggal'], 8, 2);
	            			$bulan = substr($data['kridit_tanggal'], 5, 2);
	            			$tahun = substr($data['kridit_tanggal'], 0, 4);
	            			$tanggal_beli = "$tanggal-$bulan-$tahun";
	            		}
	            	?>
	            	<?= htmlspecialchars_decode($tanggal_beli); ?>
	            </td>
	            <td><?= htmlspecialchars_decode($data["fotokopi_ktp"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["fotokopi_kk"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["fotokopi_slip_gaji"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["jumlah_cicilan"]); ?> Bulan</td>
	            <td class="text-center bg-warning">
					<a class="btn btn-success btn-xs" href="?page=beli_kridit&action=print_belikridit&kridit_kode=<?= $data['kridit_kode']; ?>" data-toggle="tooltip" title="Cetak invoice">
						<i class="fa fa-print"></i>
					</a>
	            	<a class="btn btn-primary btn-xs" href="?page=beli_kridit&action=update_belikridit&kridit_kode=<?= $data['kridit_kode']; ?>&no_ktp=<?= $data['pembeli_no_ktp']; ?>&kode_motor=<?= $data['motor_kode']; ?>" data-toggle="tooltip" title="Edit Pembelian">
			        	<i class="glyphicon glyphicon-edit"></i>
			        </a>
					<a class="btn btn-danger btn-xs" href="?page=beli_kridit&action=delete_belikridit&kridit_kode=<?= $data['kridit_kode']; ?>" onclick="return confirm('Yakin ingin hapus data ?');" data-toggle="tooltip" title="Hapus data">
						<i class="glyphicon glyphicon-trash"></i>
					</a>
	            </td>
	          </tr>
	          <?php endforeach; ?>
	        </table>
	      </div>
	      <!-- /.box-body -->
	      <!-- /.box-body -->

	      <?php
	      	$sql = "SELECT * FROM tbl_beli_kridit";
	      	$query = $conn->query($sql);
	      	$jml_data = mysqli_num_rows($query);
	      	$jml_halaman = ceil($jml_data/$batas);
	      ?>

	      <div class="box-footer clearfix">
	      	<span class="text-left">Jumlah data</span> <span class="label label-warning"><?= $jml_data;?></span>

			<ul class="pagination pagination-sm no-margin pull-right">
	      	<?php
	      		for ($i=1; $i <= $jml_halaman; $i++) {
	      			if ($i != $halaman) {
	      				?>
						<li><a href="?page=pembeli&halaman=<?=$i;?>"><?=$i;?></a></li>
	      				<?php
	      			} else {
	      				?>
						<li class="active"><a href="#"><?=$i;?></a></li>
	      				<?php
	      			}
	      		}
	      	?>
	        </ul>
	      </div>
	    <!-- /.box -->
	  </div>
	</div>
