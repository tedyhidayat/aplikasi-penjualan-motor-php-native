<?php
@session_start();
include '../_config/connection.php';

if($_SESSION['Administrator']){
?>

<?php
    if ($_SESSION['Administrator']){
            $hak = $_SESSION['Administrator'];
        }
        $query = "SELECT * FROM tbl_akun WHERE id = '$hak'";
        $sql_user = $conn->query($query);
        $data_user = mysqli_fetch_array($sql_user);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dashboard - Administrator APP PENJUALAN MOTOR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../_assets/_assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../_assets/_assets/dist/css/skins/_all-skins.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../_assets/_assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <!-- Morris chart -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../_assets/_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green-light sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="?page=" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Penjualan Motor</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../_assets/_assets/dist/img/faculty.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $data_user['nama']; ?></span>
            </a>
            <ul class="dropdown-menu"  style="box-shadow: 0px 0px 15px black;">
              <!-- User image -->
              <li class="user-header">
                <img src="../_assets/_assets/dist/img/faculty.png" class="img-circle" alt="User Image">

                <p>
                  <?php echo $data_user['nama']; ?>
                  <small><?php echo $data_user['hak']; ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer" style="background-color: #000;">
                <!-- <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div> -->
                <div class="text-center">
                  <a href="../_auth/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../_assets/_assets/dist/img/faculty.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $data_user['nama']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGASI</li>
        <li class="active"><a href="../index.html"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Pembeli</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="?page=pembeli"><i class="fa fa-tasks"></i> Lihat Data Pembeli</a></li>
            <li><a href="?page=pembeli&action=create_pembeli"><i class="fa fa-plus-square"></i> Tambah Data Pembeli</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-motorcycle"></i>
            <span>Motor</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="?page=motor"><i class="fa fa-tasks"></i> Lihat Data Motor</a></li>
            <li><a href="?page=motor&action=create_motor"><i class="fa fa-plus-square"></i> Tambah Data Motor</a></li>
          </ul>
        </li>
        <li><a href="?page=penjualan"><i class="fa fa-money"></i> <span>Penjualan</span></a></li>
        <li class="header">REPORT</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-circle-o text-red"></i>
            <span>Report Penjualan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="?page=report&action=belicash"><i class="fa fa-"></i> CASH</a></li>
            <li><a href="?page=report&action=belikridit"><i class="fa fa-"></i> KRIDIT</a></li>
          </ul>
        </li>
        <li><a href="?page=report&action=motor"><i class="fa fa-circle-o text-yellow"></i> <span>Report Stok Motor</span></a></li>
        <!-- <li class="header">INFORMASI</li> -->
        <!-- <li><a href="#"><i class="fa fa-info text-aqua"></i> <span>Tentang Aplikasi</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper">
    <?php
      include_once 'content.php';
    ?>
  </div>
    <footer class="main-footer" style="background-color: #EBEBEB;">
      <div class="pull-right hidden-xs">
        <b>SMK NEGERI 2 KOTA BEKASI</b>
      </div>
      <strong>Copyright &copy; 2018 <a href="#">Tedy Hidayat</a>.</strong>
    </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->



<!-- CK Editor -->
<script src="../_assets/_assets/bower_components/ckeditor/ckeditor.js"></script>
<!-- jQuery 3 -->
<script src="../_assets/_assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../_assets/_assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="../_assets/_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../_assets/_assets/bower_components/raphael/raphael.min.js"></script>
<script src="../_assets/_assets/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../_assets/_assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../_assets/_assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../_assets/_assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../_assets/_assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../_assets/_assets/bower_components/moment/min/moment.min.js"></script>
<script src="../_assets/_assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../_assets/_assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../_assets/_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../_assets/_assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../_assets/_assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../_assets/_assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../_assets/_assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../_assets/_assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../_assets/_assets/dist/js/demo.js"></script>



<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('ckeditor', {uiColor: '#5775D5;'})
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>

</body>
</html>

<?php
}
else{
    header("location: ../_auth/login.php");
}
?>