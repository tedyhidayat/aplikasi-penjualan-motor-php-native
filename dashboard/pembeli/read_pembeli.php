<?php

// batas pagination
$batas = 10;
$halaman = @$_GET['halaman'];

if (empty($halaman)) {
	$posisi = 0;
	$halaman = 1;
} else {
	$posisi = ($halaman - 1) * $batas;
}

// var search

if (isset($_POST['search'])) {
	$keyword = $_POST['table_search'];
	$sql = "SELECT * FROM tbl_pembeli WHERE pembeli_no_ktp LIKE '%$keyword%' or pembeli_nama LIKE '%$keyword%' or pembeli_alamat LIKE '%$keyword%' ORDER BY pembeli_nama ASC LIMIT $posisi,$batas ";
	$read_pembeli = $conn->query($sql);
	$res = mysqli_num_rows($read_pembeli);
} else {
	$sql = "SELECT * FROM tbl_pembeli WHERE pembeli_no_ktp ORDER BY pembeli_nama ASC LIMIT $posisi,$batas";
	$read_pembeli = $conn->query($sql);
	$res = mysqli_num_rows($read_pembeli);
}

?>



<section class="content-header">
  <h1>
    Pembeli
    <small><?= date('D, d/m/Y');?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Pembeli</a></li>
    <li class="active">Data pembeli</li>
  </ol>
</section>
<section class="content">
	<div class="row">
	  <div class="col-xs-12">
	    <div class="box">
	      <div class="box-header" style="padding-bottom: 20px;">
	        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#tambah_pembeli" style="border-radius: 0px;"><i class="fa fa-user-plus"></i> Tambah Data</button>

	        <div class="box-tools">
				<form action="" method="post">
		          <div class="input-group input-group-sm" style="width: 170px;">
		            <input type="text" name="table_search" class="form-control pull-right" placeholder="Masukan keyword" autocomplete="off">

		            <div class="input-group-btn">
		              <button type="submit" name="search" class="btn btn-default"><i class="fa fa-search"></i></button>
		            </div>
		          </div>
				</form>
	        </div>
	      </div>
	      <!-- /.box-header -->
	      <div class="box-body table-responsive no-padding">
	        <table class="table table-hover table-striped">
	          <tr class="alert-danger" style="border-bottom: 2px solid gray;">
	            <th width="100">NO KTP</th>
	            <th>Nama</th>
	            <th>Alamat</th>
	            <th>No Telepon</th>
	            <th>No Hp</th>
	            <th>Opsi</th>
	          </tr>

		      <?php foreach( $read_pembeli as $data ) : ?>
	          <tr>
	            <td><?= htmlspecialchars_decode($data["pembeli_no_ktp"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["pembeli_nama"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["pembeli_alamat"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["pembeli_telpon"]); ?></td>
	            <td><?= htmlspecialchars_decode($data["pembeli_hp"]); ?></td>
	            <td>
	            	<a class="btn btn-primary btn-sm" href="?page=pembeli&action=update_pembeli&ktp=<?= $data['pembeli_no_ktp']; ?>">
			        	<i class="glyphicon glyphicon-edit"></i>
			        </a>
					<a class="btn btn-danger btn-sm" href="?page=pembeli&action=delete_pembeli&ktp=<?= $data['pembeli_no_ktp']; ?>" onclick="return confirm('Yakin ingin hapus data ?');">
						<i class="glyphicon glyphicon-trash"></i>
					</a>
	            </td>
	          </tr>
	          <?php endforeach; ?>
	        </table>
	      </div>
	      <!-- /.box-body -->
	      <!-- /.box-body -->

	      <?php
	      	$sql = "SELECT * FROM tbl_pembeli";
	      	$query = $conn->query($sql);
	      	$jml_data = mysqli_num_rows($query);
	      	$jml_halaman = ceil($jml_data/$batas);
	      ?>

	      <div class="box-footer clearfix">
	      	<span class="text-left">Jumlah data</span> <span class="label label-warning"><?= $jml_data;?></span>

			<ul class="pagination pagination-sm no-margin pull-right">
	      	<?php
	      		for ($i=1; $i <= $jml_halaman; $i++) {
	      			if ($i != $halaman) {
	      				?>
						<li><a href="?page=pembeli&halaman=<?=$i;?>"><?=$i;?></a></li>
	      				<?php
	      			} else {
	      				?>
						<li class="active"><a href="#"><?=$i;?></a></li>
	      				<?php
	      			}
	      		}
	      	?>
	        </ul>
	      </div>
	    </div>
	    <!-- /.box -->
	  </div>
	</div>


	<!-- Tambah data pembeli -->
	<div class="modal fade" id="tambah_pembeli">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-user-plus"></i> Tambah data pembeli</h4>
	      </div>
	      <div class="modal-body">
	        <?php include 'create_pembeli.php'; ?>
	      </div>
	    </div>
	    <!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	<!-- /.tambah data pembeli -->

	<!-- Tambah data pembeli -->
	<div class="modal fade" id="update_pembeli">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-user-plus"></i> Tambah data pembeli</h4>
	      </div>
	      <div class="modal-body">
	        <?php include 'update_pembeli.php'; ?>
	      </div>
	    </div>
	    <!-- /.modal-content -->
	  </div>
	  <!-- /.modal-dialog -->
	</div>
	<!-- /.tambah data pembeli -->
</section>