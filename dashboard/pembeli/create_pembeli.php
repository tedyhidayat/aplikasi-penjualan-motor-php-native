<section class="content-header">
  <h1>
    Pembeli
    <small><?= date('D, d/m/Y');?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Pembeli</a></li>
    <li class="active">Tambah pembeli</li>
  </ol>
</section>
<section class="content">
	<!-- general form elements disabled -->
	<div class="box box-success">
	  <!-- /.box-header -->
	  <div class="box-body">
	  	<div class="col-md-10 col-md-offset-1"  style="padding-bottom: 30px;">
		    <form role="form" method="post" action="">
		      <div class="form-group has-warning">
		        <label for="ktp">Nomor KTP</label>
		        <input type="number" name="noktp" class="form-control" id="ktp" placeholder="Masukan nomor KTP anda" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="nama">Nama</label>
		        <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukan nama" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label>Alamat</label>
		        <textarea id="ckeditor" name="alamat" class="form-control" rows="10" placeholder="Masukan alamat" autocomplete="off"></textarea>
		      </div>
		      <div class="form-group">
		        <label for="tlp">Nomor yang dapat dihubungi</label>
		        <input type="number" name="tlp" class="form-control" id="tlp" placeholder="No Telepon" autocomplete="off">
		        <br>
		        <input type="number" name="hp" class="form-control" id="tlp" placeholder="No HP" autocomplete="off">
		      </div>
		</div>
	  </div>
	  <!-- /.box-body -->
	  <!-- .box-footer -->
	  <div class="box-footer text-right">
	  	<div class="col-md-10 col-md-offset-1">
	  		<button type="reset" name="reset" class="btn btn-danger text-left" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i> Cancel</button>
	    	<button type="submit" name="tambah" class="btn btn-success text-right"><i class="fa fa-plus"></i> Tambah</button>
	    	</form>
	    </div>
	  </div>
	</div>
	<!-- /.box -->
</section>

<?php

if (isset($_POST['tambah'])) {

	$no_ktp	= htmlspecialchars($_POST['noktp']);
	$nama 	= htmlspecialchars($_POST['nama']);
	$alamat = htmlspecialchars($_POST['alamat']);
	$tlp 	= htmlspecialchars($_POST['tlp']);
	$hp 	= htmlspecialchars($_POST['hp']);

	if ($no_ktp == "" || $nama == "" || $alamat == "" || $hp == "") {
		?>
			<script type="text/javascript">
				alert('Data tidak boleh kosong !');
				window.location.href="?page=pembeli";
			</script>
		<?php
	} else {
		$sql = "INSERT INTO tbl_pembeli (pembeli_no_ktp,pembeli_nama,pembeli_alamat,pembeli_telpon,pembeli_hp) VALUES ('$no_ktp','$nama','$alamat','$tlp','$hp')";
		$insert = $conn->query($sql);

		if ($insert > 0) {
			?>
				<script type="text/javascript">
					alert('Data berhasil ditambahkan.');
					window.location.href="?page=pembeli";
				</script>
			<?php
		} else {
			echo "Data gagal tersimpan !";
		}

		$conn->close();
	}
}

?>
