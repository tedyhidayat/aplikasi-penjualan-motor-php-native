<?php
	$ktp			= $_GET['ktp'];
	$sql			= $conn->query("SELECT * FROM tbl_pembeli WHERE pembeli_no_ktp ='$ktp'") or die (mysqli_error());
	$data 			= mysqli_fetch_array($sql);
?>


<section class="content-header">
  <h1>
    Pembeli
    <small><?= date('D, d/m/Y');?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Pembeli</a></li>
    <li class="active">Edit data pembeli</li>
  </ol>
</section>

<section class="content">
	<!-- general form elements disabled -->
	<div class="box box-success">
	  <!-- /.box-header -->
	  <div class="box-body">
	  	<div class="col-md-10 col-md-offset-1"  style="padding-bottom: 30px;">
		    <form role="form" method="post" action="">
		      <div class="form-group has-warning">
		        <label for="ktp">Nomor KTP</label>
		        <input type="text" name="noktp" class="form-control" id="ktp" value="<?= $data["pembeli_no_ktp"]; ?>" readonly autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label for="nama">Nama</label>
		        <input type="text" name="nama" class="form-control" id="nama" value="<?= htmlspecialchars_decode($data["pembeli_nama"]); ?>" autocomplete="off">
		      </div>
		      <div class="form-group">
		        <label>Alamat</label>
		        <textarea id="ckeditor" name="alamat" class="form-control" rows="10" autocomplete="off"><?= htmlspecialchars_decode($data["pembeli_alamat"]); ?></textarea>
		      </div>
		      <div class="form-group">
		        <label for="tlp">Nomor yang dapat dihubungi</label>
		        <input type="number" name="tlp" class="form-control" id="tlp" value="<?= htmlspecialchars_decode($data["pembeli_telpon"]); ?>" autocomplete="off">
		        <br>
		        <input type="number" name="hp" class="form-control" id="tlp" value="<?= htmlspecialchars_decode($data["pembeli_hp"]); ?>" autocomplete="off">
		      </div>
		</div>
	  </div>
	  <!-- /.box-body -->
	  <!-- .box-footer -->
	  <div class="box-footer text-right">
	  	<div class="col-md-10 col-md-offset-1">
	  		<a class="btn btn-danger text-left" href="?page=pembeli"><i class="fa fa-remove"></i> Cancel</a>
	    	<button type="submit" name="update" class="btn btn-primary text-right"><i class="fa fa-edit"></i> Edit</button>
	    	</form>
	    </div>
	  </div>
	</div>
	<!-- /.box -->
</section>

<?php

if (isset($_POST['update'])) {

	$no_ktp	= htmlspecialchars($_POST['noktp']);
	$nama 	= htmlspecialchars($_POST['nama']);
	$alamat = htmlspecialchars($_POST['alamat']);
	$tlp 	= htmlspecialchars($_POST['tlp']);
	$hp 	= htmlspecialchars($_POST['hp']);

	$sql = "UPDATE tbl_pembeli SET pembeli_nama='$nama',pembeli_alamat='$alamat',pembeli_telpon='$tlp',pembeli_hp='$hp' WHERE pembeli_no_ktp='$no_ktp'";
	$update = $conn->query($sql);

	if ($update > 0) {
		?>
			<script type="text/javascript">
				alert('Data berhasil diperbarui.');
				window.location.href="?page=pembeli";
			</script>
		<?php
	} else {
		echo "Data gagal diperbarui !";
	}

	$conn->close();

}

?>
