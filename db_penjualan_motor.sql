-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2018 at 01:08 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_penjualan_motor`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akun`
--

CREATE TABLE `tbl_akun` (
  `id` varchar(10) NOT NULL,
  `nama` char(20) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `j_kelamin` enum('L','P') NOT NULL,
  `foto` varchar(100) NOT NULL,
  `hak` enum('Administrator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_akun`
--

INSERT INTO `tbl_akun` (`id`, `nama`, `username`, `password`, `j_kelamin`, `foto`, `hak`) VALUES
('AC001', 'Tedy', 'admin', 'admin', 'L', '', 'Administrator'),
('AC002', 'Karina', 'admin2', 'admin2', 'P', '', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bayar_cicilan`
--

CREATE TABLE `tbl_bayar_cicilan` (
  `cicilan_kode` char(20) NOT NULL,
  `kridit_kode` char(20) NOT NULL,
  `cicilan_tanggal` date NOT NULL,
  `cicilan_jumlah` float(8,0) NOT NULL,
  `cicilan_ke` float(8,0) NOT NULL,
  `cicilan_sisa_ke` float(8,0) NOT NULL,
  `cicilan_sisa_harga` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_beli_cash`
--

CREATE TABLE `tbl_beli_cash` (
  `cash_kode` char(20) NOT NULL,
  `pembeli_no_ktp` varchar(17) NOT NULL,
  `motor_kode` char(10) NOT NULL,
  `cash_tanggal` date NOT NULL,
  `cash_bayar` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_beli_cash`
--

INSERT INTO `tbl_beli_cash` (`cash_kode`, `pembeli_no_ktp`, `motor_kode`, `cash_tanggal`, `cash_bayar`) VALUES
('CASH/20102018/00001', '123456789010', 'MTR001', '2018-10-20', 1000000),
('CASH/21102018/00002', '123456789019', 'MTR003', '2018-10-21', 1000000000),
('CASH/31102018/00003', '123456789010', 'MTR003', '2018-10-31', 12000000),
('CASH/31102018/00004', '109056789009', 'MTR002', '2018-10-31', 100000000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_beli_kridit`
--

CREATE TABLE `tbl_beli_kridit` (
  `kridit_kode` char(20) NOT NULL,
  `pembeli_no_ktp` varchar(17) NOT NULL,
  `motor_kode` char(10) NOT NULL,
  `kridit_tanggal` date NOT NULL,
  `fotokopi_ktp` tinyint(1) NOT NULL,
  `fotokopi_kk` tinyint(1) NOT NULL,
  `fotokopi_slip_gaji` tinyint(1) NOT NULL,
  `jumlah_cicilan` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_beli_kridit`
--

INSERT INTO `tbl_beli_kridit` (`kridit_kode`, `pembeli_no_ktp`, `motor_kode`, `kridit_tanggal`, `fotokopi_ktp`, `fotokopi_kk`, `fotokopi_slip_gaji`, `jumlah_cicilan`) VALUES
('KRIDIT/0111201800001', '098765432112', 'MTR002', '2018-11-01', 1, 1, 1, '12'),
('KRIDIT/0111201800002', '109056789009', 'MTR003', '2018-11-01', 1, 1, 1, '24'),
('KRIDIT/0111201800003', '123456789010', 'MTR002', '2018-11-01', 0, 0, 0, '36'),
('KRIDIT/0111201800004', '1234567891001', 'MTR002', '2018-11-01', 1, 1, 1, '12'),
('KRIDIT/0111201800005', '12345678811', 'MTR003', '2018-11-01', 0, 0, 0, '12'),
('KRIDIT/0111201800006', '123456789010', 'MTR003', '2018-11-01', 1, 1, 1, '12'),
('KRIDIT/0111201800007', '123456789889', 'MTR004', '2018-11-01', 1, 1, 1, '36'),
('KRIDIT/0111201800008', '123456789845', 'MTR002', '2018-11-01', 1, 0, 0, '12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_motor`
--

CREATE TABLE `tbl_motor` (
  `motor_kode` char(10) NOT NULL,
  `motor_merk` varchar(25) NOT NULL,
  `motor_type` varchar(15) NOT NULL,
  `motor_warna_pilihan` varchar(70) NOT NULL,
  `motor_harga` double NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_motor`
--

INSERT INTO `tbl_motor` (`motor_kode`, `motor_merk`, `motor_type`, `motor_warna_pilihan`, `motor_harga`, `gambar`) VALUES
('MTR001', 'Mio Z', 'Automatic', 'Merah', 15000000, '5bb06f5fbaa372.82124578.JPG'),
('MTR002', 'Ninja 250 R Monoshock', 'Sport', 'Hijau', 100000000, '5bb2d7bdf21695.22489407.JPG'),
('MTR003', 'Honda Vario 150 New', 'Automatic', 'Hitam', 25000000, '5bb2bebe390278.53486611.PNG'),
('MTR004', 'Honda CBR 150R', 'Sport', 'Orange', 35000000, '5bb2bef09e4ca5.81046742.PNG'),
('MTR005', 'Suzuki GSX 150R', 'Sport', 'Biru', 30000000, '5bb2d6c53455c1.91800377.JPG'),
('MTR006', 'Honda Beat FI', 'Automatic', 'Merah', 14000000, '5bb2d57f6f81b5.25404907.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pembeli`
--

CREATE TABLE `tbl_pembeli` (
  `pembeli_no_ktp` varchar(17) NOT NULL,
  `pembeli_nama` varchar(25) NOT NULL,
  `pembeli_alamat` varchar(60) NOT NULL,
  `pembeli_telpon` char(15) NOT NULL,
  `pembeli_hp` char(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pembeli`
--

INSERT INTO `tbl_pembeli` (`pembeli_no_ktp`, `pembeli_nama`, `pembeli_alamat`, `pembeli_telpon`, `pembeli_hp`) VALUES
('098765432112', 'Sutriono', '&lt;p&gt;Jawa Timur&lt;/p&gt;\r\n', '989987', '088818898767'),
('109056789009', 'Roy', '&lt;p&gt;Bengkulu&lt;/p&gt;\r\n', '', '087665756449'),
('12345678811', 'Bambang Pamungkas', '&lt;p&gt;Jakarta&amp;nbsp;&lt;/p&gt;\r\n', '1231231', '089676445543'),
('123456789010', 'Tedy Hidayat ', '&lt;p&gt;Bekasi Timur&lt;/p&gt;\r\n', '117876156', '081288940592'),
('123456789012', 'Roy Martin', '&lt;p&gt;Jakarta Barat&lt;/p&gt;\r\n', '', '089676448840'),
('123456789016', 'Maman Suherman', '&lt;p&gt;Jawa Barat&lt;/p&gt;\r\n', '', '085212228898'),
('123456789019', 'Arya Wiguna', '&lt;p&gt;Sulawesi selatan&lt;/p&gt;\r\n', '91102453', '087877886676'),
('1234567891001', 'Bambang Sucipto', '&lt;p&gt;Bandung&lt;/p&gt;\r\n', '22344234', '085212228356'),
('123456789845', 'Alberto Nimero', '&lt;p&gt;Papua Barat&lt;/p&gt;\r\n', '', '08881234567'),
('123456789889', 'Cut Sukmayah', '&lt;p&gt;Aceh&lt;/p&gt;\r\n', '', '081233456789');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_akun`
--
ALTER TABLE `tbl_akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_bayar_cicilan`
--
ALTER TABLE `tbl_bayar_cicilan`
  ADD PRIMARY KEY (`cicilan_kode`),
  ADD KEY `kridit_kode` (`kridit_kode`);

--
-- Indexes for table `tbl_beli_cash`
--
ALTER TABLE `tbl_beli_cash`
  ADD PRIMARY KEY (`cash_kode`),
  ADD KEY `pembeli_no_ktp` (`pembeli_no_ktp`),
  ADD KEY `motor_kode` (`motor_kode`);

--
-- Indexes for table `tbl_beli_kridit`
--
ALTER TABLE `tbl_beli_kridit`
  ADD PRIMARY KEY (`kridit_kode`),
  ADD KEY `pembeli_no_ktp` (`pembeli_no_ktp`),
  ADD KEY `motor_kode` (`motor_kode`);

--
-- Indexes for table `tbl_motor`
--
ALTER TABLE `tbl_motor`
  ADD PRIMARY KEY (`motor_kode`);

--
-- Indexes for table `tbl_pembeli`
--
ALTER TABLE `tbl_pembeli`
  ADD PRIMARY KEY (`pembeli_no_ktp`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_bayar_cicilan`
--
ALTER TABLE `tbl_bayar_cicilan`
  ADD CONSTRAINT `tbl_bayar_cicilan_ibfk_1` FOREIGN KEY (`kridit_kode`) REFERENCES `tbl_beli_kridit` (`kridit_kode`);

--
-- Constraints for table `tbl_beli_cash`
--
ALTER TABLE `tbl_beli_cash`
  ADD CONSTRAINT `tbl_beli_cash_ibfk_1` FOREIGN KEY (`pembeli_no_ktp`) REFERENCES `tbl_pembeli` (`pembeli_no_ktp`),
  ADD CONSTRAINT `tbl_beli_cash_ibfk_2` FOREIGN KEY (`motor_kode`) REFERENCES `tbl_motor` (`motor_kode`);

--
-- Constraints for table `tbl_beli_kridit`
--
ALTER TABLE `tbl_beli_kridit`
  ADD CONSTRAINT `tbl_beli_kridit_ibfk_2` FOREIGN KEY (`motor_kode`) REFERENCES `tbl_motor` (`motor_kode`),
  ADD CONSTRAINT `tbl_beli_kridit_ibfk_3` FOREIGN KEY (`pembeli_no_ktp`) REFERENCES `tbl_pembeli` (`pembeli_no_ktp`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
