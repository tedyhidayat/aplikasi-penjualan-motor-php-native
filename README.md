Aplikasi showroom motor sederhana berbasis web. Ini adalah tugas projek saya saat SMK.

Fitur :
- CRUD data motor
- CRUD Stok motor
- Cetak invoice
- Cetak Laporan
- Authentication

Teknologi :
- PHP 5
- Admin LTE
- HTML
- CSS
- Javascript