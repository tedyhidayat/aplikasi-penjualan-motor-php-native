<?php
@session_start();
include '../_config/connection.php';

if(isset($_SESSION['Administrator'])){
    header("location: ../index.html");
}
else{
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LOGIN | Penjualan Motor</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../_assets/_assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../_assets/_assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../_assets/_assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../_assets/_assets/index2.html"><b>LOGIN</b> Admin</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="border-radius: 5px;">

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" autocomplete="off">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <p><a href="register.php" class="text-center">Register</a></p>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="login">Masuk</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->


  <!-- PHP CODE LOGIN -->

  <?php

  if(isset($_POST['login'])){

    $username   = htmlspecialchars($_POST['username']);
    $password   = htmlspecialchars($_POST['password']);

      if($username == "" || $password == ""){
          ?>
              <script type="text/javascript">
                  alert("Username or password tidak boleh kosong !");
                  window.location.href ="login.php";
              </script>
          <?php
      }
      else{
          $sql = mysqli_query($conn,"SELECT * FROM  tbl_akun WHERE username='$username' and password='$password'") or die (mysqli_error($conn));
          $data_user = mysqli_fetch_array($sql);
          $cek_login = mysqli_num_rows($sql);
          if($cek_login >= 1){
              if($data_user['hak'] == "Administrator"){
                  $_SESSION['Administrator'] = $data_user['id'];
                  ?>
                  <script type="text/javascript">
                    alert("Selamat datang <?=$data_user['nama']?>");
                    window.location.href="../index.html";
                  </script>
                  <?php
              }
          }
          else{
              ?>
              <div class="col-sm-12" style="margin-top: 10px;">
                  <div class="alert alert-danger alert-dismissable" role="alert">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                      <strong>Login Gagal !</strong> Username atau Password salah.
                  </div>
              </div>
              <?php
          }
      }
  }

  ?>

  <!-- END CODE LOGIN -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../_assets/_assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../_assets/_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<!-- <script src="../_assets/_assets/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script> -->
</body>
</html>


<?php
}
?>